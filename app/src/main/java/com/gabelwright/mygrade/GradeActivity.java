package com.gabelwright.mygrade;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

public class GradeActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);

        WebClient web = new WebClient(new WebClient.GradeListener() {

            /** Called when a requested grade is found. */
            public void onGrade(String date, Grade grade) {
                loadScores(date, grade);
            }

            /** Called when an error is encountered. */
            public void onError(String msg) {
                Intent data = new Intent();
                data.setData(Uri.parse(msg));
                setResult(RESULT_CANCELED, data);
                finish();
            }
        });

        String username = getIntent().getStringExtra("username");
        String pin = getIntent().getStringExtra("pin");

        web.queryAsync(username, pin);

    }

    private void loadScores(String date, Grade grade){
        TextView gDate = findViewById(R.id.gradeDate);
        TextView gGrade = findViewById(R.id.gradeGrade);
        TextView gWGrade = findViewById(R.id.gradeWGrade);

        gDate.setText(date);
        gGrade.setText(grade.grade);
        gWGrade.setText(grade.total + "");

        ScoreListAdapter s = new ScoreListAdapter(this, grade.scores());
        listView = findViewById(R.id.gradeListView);
        listView.setAdapter(s);
    }

    @Override
    public void onBackPressed() {
        Intent data = new Intent();
        data.setData(Uri.parse(""));
        setResult(RESULT_OK, data);
        finish();
    }

    private static class ScoreListAdapter extends ArrayAdapter<Grade.Score> {

        private final List<Grade.Score> scores;

        public ScoreListAdapter(Context ctx, List<Grade.Score> scores) {
            super(ctx, -1, scores);
            this.scores = scores;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView != null ? convertView
                    : LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.grade_detail_row, parent, false);
            Grade.Score score = scores.get(position);
            TextView view = row.findViewById(R.id.nameView);
            view.setText(score.name);
            view = row.findViewById(R.id.maxView);
            view.setText(Integer.toString(score.max));
            view = row.findViewById(R.id.earnedView);
            view.setText(Integer.toString(score.earned));
            return row;
        }
    }

}
