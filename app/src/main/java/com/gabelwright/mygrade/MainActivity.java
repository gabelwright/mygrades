package com.gabelwright.mygrade;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView usernameView;
    TextView pinView;
    CheckBox remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
//        int highScore = sharedPref.getInt(getString(R.string.saved_high_score_key), defaultValue);
        String username = sharedPref.getString("username", "");
        String pin = sharedPref.getString("pin", "");

        usernameView = findViewById(R.id.usernameInput);
        pinView = findViewById(R.id.passwordInput);
        remember = findViewById(R.id.remeber_checkbox);

        usernameView.setText(username);
        pinView.setText(pin);

        if(!username.isEmpty() || !pin.isEmpty())
            remember.setChecked(true);

    }

    public void checkGrades(View view){
        try {
            String username = usernameView.getText().toString();

            String pin = pinView.getText().toString();

            boolean isRemembered = remember.isChecked();

            if(username.equals("") || pin.equals("")){
                Toast.makeText(this, "Username and Pin are required", Toast.LENGTH_LONG).show();
            }else{
                if(isRemembered)
                    storeCred(username, pin);
                else
                    forget();

                Intent k = new Intent(this, GradeActivity.class);
                k.putExtra("username", username);
                k.putExtra("pin", pin);
                startActivityForResult(k, 1);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void storeCred(String username, String pin){
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("username", username);
        editor.putString("pin", pin);
        editor.commit();
    }

    private void forget(){
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("username", "");
        editor.putString("pin", "");
        editor.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, data.getData().toString(), Toast.LENGTH_LONG).show();
        }else{

        }


    }
}
